#!/bin/bash

RED='\033[0;31m'
GREEN='\033[0;32m'
BLUE='\033[0;34m'
NC='\033[0m' # No Color

# check .git directory
if [ ! -d .git ]; then
    echo -e "${RED}This is not a git repository!${NC}"
    exit 1
fi

# Install the pre-commit hook
echo -en "${BLUE}Installing pre-commit hook...\t${NC}"
pip install pre-commit >> /dev/null
pre-commit install >> /dev/null

wget https://gitlab.com/yeongjun.park/hook-test/-/raw/main/CPPLINT.cfg -O CPPLINT.cfg >> /dev/null 2>&1
wget https://gitlab.com/yeongjun.park/hook-test/-/raw/main/.pre-commit-config.yaml -O .pre-commit-config.yaml >> /dev/null 2>&1


# check if pre-commit is installed(file check)
if [ -f .git/hooks/pre-commit ]; then
    echo -e "${GREEN}SUCCESS!${NC}"
else
    echo -e "${RED}FAILED!${NC}"
fi


# Install the commit-msg hook
echo -en "${BLUE}Installing commit-msg hook...\t${NC}"
wget https://gitlab.com/yeongjun.park/hook-test/-/raw/main/hooks/verify_commit_msg.py -O .git/hooks/commit-msg >> /dev/null 2>&1
chmod +x .git/hooks/commit-msg
if [ -f .git/hooks/commit-msg ]; then
    echo -e "${GREEN}SUCCESS!${NC}"
else
    echo -e "${RED}FAILED!${NC}"
fi

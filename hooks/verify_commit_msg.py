#!/usr/bin/env python3
"""
file: verify_commit_msg.py
description: This script is used to verify the commit message format.
author: Yeongjun Park
  - Ref: https://popawaw.tistory.com/325
email: yeongjun.park@aceworks.ai
last-modified: 2024-04-15
version: 0.0.1

Commit Rule:
  1. <Type>: <Subject>
    - Subject should be less than 50 characters.
    - If using body, it should be separated by a blank line.
    - If using body, it should be less than 72 characters.
  2. Type:
    - feat: A new feature
    - fix: A bug fix
    - docs: Documentation only changes
    - style: Changes that do not affect the meaning of the code (white-space, formatting, missing semi-colons, etc)
    - refactor: A code change that neither fixes a bug nor adds a feature
    - chore: Changes to the build process or auxiliary tools and libraries such as documentation generation
  3. Commit Rule
    - Capitalize the subject line
    - Do not end the subject line with a period
    - Use the imperative mood in the subject line
    - Separate subject from body with a blank line
  4. Commit Message Example
    feat(yeongjun): Add new feature #1
"""
import re
import sys
import os
COMMIT_HEADER_MESSAGE_LENGTH = 50
COMMIT_BODY_MESSAGE_LENGTH = 72
RETURN_FAILURE = 1
RETURN_SUCCESS = 0

types = ["feat", "fix", "docs", "style", "refactor", "chore"]
regax = re.compile(r"^(feat|fix|docs|style|refactor|chore)(\([a-zA-Z0-9]+\))?: .+$")
not_allowable_branches = ["master", "main", "develop", "dev", "release", "hotfix", "Merge"]

class bash_colors:
    """bash_colors
    It defines the color codes for the terminal.
    """
    RED = "\033[91m"
    GREEN = "\033[92m"
    YELLOW = "\033[93m"
    BLUE = "\033[94m"
    NC = "\033[0m"
    BG_NC = "\033[49m"


def check_commit_header(header):
    """ check_commit_header
    It checks the commit message header format.
    1. Check the length of the commit message header.
    2. Check the format of the commit message header.

    Args:
        header (str): The commit message header.
    """

    sys.stderr.write(bash_colors.BLUE + "\t- Commit Message Length...\t\t" + bash_colors.NC)
    # If the commit message is merge message, skip the verification.
    if header.startswith("Merge"):
        sys.stderr.write(bash_colors.GREEN + "OK!\n" + bash_colors.NC)
        sys.exit(RETURN_SUCCESS)
    if len(header) > COMMIT_HEADER_MESSAGE_LENGTH:
        sys.stderr.write(
            bash_colors.RED
            + "Error!\n"
            + "----------------------------------------------------------------"
            + "\nError: Commit message header is too long.\n"
            + bash_colors.NC
        )
        sys.stderr.write(
            bash_colors.RED
            + "       It should be less than "
            + str(COMMIT_HEADER_MESSAGE_LENGTH)
            + " characters.\n"
            + bash_colors.NC
        )
        sys.exit(RETURN_FAILURE)
    sys.stderr.write(bash_colors.GREEN + "OK!\n" + bash_colors.NC)
    sys.stderr.write(bash_colors.BLUE + "\t- Commit Message Format...\t\t" + bash_colors.NC)
    if not regax.match(header):
        sys.stderr.write(
            bash_colors.RED
            + "Error!\n"
            + "----------------------------------------------------------------"
            + "\nError: Commit message header format is wrong.\n"
            + bash_colors.NC
        )
        sys.stderr.write(
            bash_colors.RED
            + "       It should be in the format of '<Type>: <Subject>'.\n"
            + bash_colors.NC
        )
        sys.stderr.write(bash_colors.RED + "       Type: " + str(types) + "\n" + bash_colors.NC)
        sys.exit(RETURN_FAILURE)
    sys.stderr.write(bash_colors.GREEN + "OK!\n" + bash_colors.NC)


def check_commit_body(body):
    """check_commit_body
    It checks the commit message body format.
    1. Check the first line of the commit message body.
    2. Check the length of the commit message body.

    Args:
        body (list): The commit message body.
    """
    sys.stderr.write(bash_colors.BLUE + "\t- Commit Message Body...\t\t" + bash_colors.NC)
    if len(body) > 1:
        if body[0] != "\n":
            sys.stderr.write(
                bash_colors.RED
                + "Error!\n"
                + "----------------------------------------------------------------"
                + "\nError: Commit message body should be separated by a blank line.\n"
                + bash_colors.NC
            )
            sys.exit(RETURN_FAILURE)
        for line in body[1:]:
            if len(line) > COMMIT_BODY_MESSAGE_LENGTH:
                sys.stderr.write(
                    bash_colors.RED
                    + "Error!\n"
                    + "----------------------------------------------------------------"
                    + "\nError: Commit message body is too long.\n"
                    + bash_colors.NC
                )
                sys.stderr.write(
                    bash_colors.RED
                    + "       It should be less than "
                    + str(COMMIT_BODY_MESSAGE_LENGTH)
                    + " characters.\n"
                    + bash_colors.NC
                )
                sys.exit(RETURN_FAILURE)
    sys.stderr.write(bash_colors.GREEN + "OK!\n" + bash_colors.NC)
def get_branch_name():
    return os.popen("git rev-parse --abbrev-ref HEAD").read().strip()
def check_branch_name():
    cur_branch = get_branch_name()
    sys.stderr.write(bash_colors.BLUE + "\t- Verify Branch Name...\t\t\t" + bash_colors.NC)
    if cur_branch in not_allowable_branches:
        sys.stderr.write(
            bash_colors.RED
            + "Error!\n"
            + "----------------------------------------------------------------"
            + "\nError: Not allowable branch name.\n"
            + bash_colors.NC
        )
        sys.stderr.write(
            bash_colors.RED
            + "       It should not be "
            + str(not_allowable_branches)
            + ".\n"
            + bash_colors.NC
        )
        sys.exit(RETURN_FAILURE)
    sys.stderr.write(bash_colors.GREEN + "OK!\n" + bash_colors.NC)

def verify_commit_msg():
    """verify_commit_msg
    It verifies the commit message.
    1. Check if the commit message is empty.
    2. Check the commit message header.
    3. Check the commit message body.
    4. Check the branch name.
    """
    sys.stderr.write(bash_colors.YELLOW + "Verify Commit Message...\n" + bash_colors.NC)
    with open(sys.argv[1], "r", encoding="utf-8") as commit:
        lines = commit.readlines()
        lines = [line for line in lines if not line.startswith("#")]

        while lines[-1] == "\n":
            lines = lines[:-1]
            if lines == []:
                break
        sys.stderr.write(
            bash_colors.BLUE + "\t- Commit Message is not empty...\t" + bash_colors.NC
        )
        if len(lines) < 1:
            sys.stderr.write(
                bash_colors.RED
                + "Error!\n"
                + "----------------------------------------------------------------"
                + "\nError: Commit message is empty.\n"
                + bash_colors.NC
            )
            sys.exit(RETURN_FAILURE)
        sys.stderr.write(bash_colors.GREEN + "OK!\n" + bash_colors.NC)

        check_commit_header(lines[0])
        check_commit_body(lines[1:])

        check_branch_name()

        sys.stderr.write(bash_colors.BLUE + "Commit Message is Verified!\n" + bash_colors.NC)


if __name__ == "__main__":
    verify_commit_msg()
    sys.exit(RETURN_SUCCESS)

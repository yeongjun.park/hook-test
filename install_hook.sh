#!/bin/bash

RED='\033[0;31m'
GREEN='\033[0;32m'
BLUE='\033[0;34m'
YELLOW='\033[1;33m'
NC='\033[0m' # No Color

# check .git directory
if [ ! -d .git ]; then
    echo -e "${RED}This is not a git repository!${NC}"
    exit 1
fi

wget https://gitlab.com/yeongjun.park/hook-test/-/raw/main/hooks/hook.sh -O hook.sh >> /dev/null 2>&1

if [ ! -s hook.sh ]; then
    echo -e "${RED}Failed to download hook install script${NC}"
    echo -e "${YELLOW}Please check your internet connection Or support the developer by email, google chat, or issue${NC}"
    echo -e "${YELLOW}Email: yeongjun.park@aceworks.ai${NC}"
    exit 1
fi
chmod +x hook.sh

./hook.sh

rm -rf hook.sh
